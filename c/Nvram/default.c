/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "nvram.h"

struct nvram_tuple router_defaults[] =
{
    { "nvramver", "3.0"},
    { "firmver", "1.0.0.1"},
    { NULL, NULL }
}; // router_defaults

// nvram for system control state
struct nvram_tuple router_state_defaults[] =
{
    { "service", ""},
    { NULL, NULL }
};
