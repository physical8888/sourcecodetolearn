/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <sys/file.h>
#include "nvram.h"

static struct nvram_tuple *nv_list=NULL;

struct nvram_tuple *find_entry(const char *name)
{
    struct nvram_tuple *p=nv_list;
    
    
    if (!name || !p)
        return NULL;
        
    while(p)
    {
        if (strcmp(p->name,name)==0) break;
        p=p->next;
    }
    return p;
}

int add_entry(const char *name,char *value)
{
    struct nvram_tuple *p;
    
    if (!value) return -1;
    
    p=find_entry(name);
    if (!name)
    {
        free(value);
        return -1;
    }
    if (p)
    {
        if (p->value) free(p->value);
        p->value = value;
    }
    else
    {
        struct nvram_tuple *new_entry;
        new_entry = malloc(sizeof(struct nvram_tuple));
        if (!new_entry)
        {
            free(value);
            return -1;
        }
        new_entry->name = strdup(name);
        if (!new_entry->name)
        {
            free(value);
            free(new_entry);
            return -1;
        }
        new_entry->value = value;
        new_entry->next=nv_list;
        nv_list = new_entry;
    }
    return 0;
}

int readFile(char *path, char **data)
{
    int total;
    int fd=0;
    
    if ((fd=open(path, O_RDONLY)) < 0)
        return -1;
        
    lockf(fd,F_LOCK,0);
    total=lseek(fd,0,SEEK_END);
    lseek(fd,0,0);
    
    if (total == -1)
    {
        lockf(fd,F_ULOCK,0);
        close(fd);
        return -1;
    }
    
    if ((*data=malloc(total))==NULL)
    {
        lockf(fd,F_ULOCK,0);
        close(fd);
        return -1;
    }
    
    if (read(fd,*data,total)<0)
    {
        free(*data);
        lockf(fd,F_ULOCK,0);
        close(fd);
        return -1;
    }
    
    lockf(fd,F_ULOCK,0);
    close(fd);
    return total;
}

void writeFile(char *path, char *data, int len)
{
    int fd;
    
    if ((fd=open(path, O_CREAT|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR)) < 0)
        return;
    lockf(fd,F_LOCK,0);
    write(fd, data, len);
    lockf(fd,F_ULOCK,0);
    close(fd);
}

static unsigned long crc32(char *data, int length)
{
    unsigned long crc, poly;
    long crcTable[256];
    int i, j;
    
    poly = 0xEDB88320L;
    for (i=0; i<256; i++)
    {
        crc = i;
        for (j=8; j>0; j--)
        {
            if (crc&1)
            {
                crc = (crc >> 1) ^ poly;
            }
            else
            {
                crc >>= 1;
            }
        }
        crcTable[i] = crc;
    }
    crc = 0xFFFFFFFF;
    while(length-- > 0)
    {
        crc = ((crc>>8) & 0x00FFFFFF) ^ crcTable[ (crc^((char)*(data++))) & 0xFF ];
    }
    return crc^0xFFFFFFFF;
}

extern int nvram_load()
{
    unsigned long crc;
    int fd1;
    char *data;
    
    nvram_header_t header;
    fd1=open(NVRAM_PATH,O_RDONLY);
    if (fd1<0) return NVRAM_FLASH_ERR;
    
    read(fd1, &header,sizeof(nvram_header_t));
    lseek(fd1,NVRAM_HEADER_SIZE,0);
    if (header.magic!=NVRAM_MAGIC)
    {
        close(fd1);
        return NVRAM_MAGIC_ERR;
    }
    
    data=malloc(header.len+1);
    read(fd1, data, header.len+1);
    close(fd1);
    
    crc=crc32(data, header.len);
    if (crc!=header.crc)
    {
        free(data);
        return NVRAM_CRC_ERR;
    }
    writeFile(NVRAM_TMP_PATH, data, header.len);
    free(data);
    return NVRAM_SUCCESS;
}

int nvram_commit()
{
    int fd1;
    char *data;
    int len;
    
    nvram_header_t header;
    
    if ((fd1=open(NVRAM_PATH,O_WRONLY | O_CREAT))<0)
        return NVRAM_FLASH_ERR;
        
    if ((len=readFile(NVRAM_TMP_PATH, &data))<=0)
        return NVRAM_SHADOW_ERR;
        
    header.magic=NVRAM_MAGIC;
    header.crc=crc32(data, len);
    header.len=len;
    
    lockf(fd1,F_LOCK,0);
    write(fd1, &header,sizeof(nvram_header_t));
    lseek(fd1,NVRAM_HEADER_SIZE,0);
    write(fd1, data, len);
    
    lseek(fd1,NVRAM_HEADER_SIZE,0);
    read(fd1, data,len);
    if (header.crc!=crc32(data, len))
    {
        lockf(fd1,F_ULOCK,0);
        close(fd1);
        free(data);
        return NVRAM_FLASH_ERR;
    }
    lockf(fd1,F_ULOCK,0);
    close(fd1);
    free(data);
    return NVRAM_SUCCESS;
}

char* nvram_get_func(const char *name,char *path)
{
    char *bufspace;
    int size;
    char *s,*sp;
    
    if ((size=readFile(path, &bufspace))<0)
        return NULL;
        
    for (s = bufspace; *s; s++)
    {
        if (!strncmp(s, name, strlen(name)) && *(s+strlen(name))=='=')
        {
            sp=malloc(strlen(s)-strlen(name));
            memcpy(sp,(s+strlen(name)+1),(strlen(s)-strlen(name)));
            free(bufspace);
            add_entry(name,sp);
            return sp;
        }
        while(*(++s));
    }
    free(bufspace);
    return NULL;
}

char* nvram_get(const char *name)
{
    char *pt;
    
    if (!strncmp(name, "other", 5))
    {
        if ((pt=nvram_get_func(name,NVRAM_OTHER_CONF_PATH))==NULL)
            return NULL;
    }
    else if ((pt=nvram_get_func(name,NVRAM_TMP_PATH))==NULL)
    {
        if ((pt=nvram_get_func(name,NVRAM_DEFAULT))!=NULL)
            nvram_set(name,pt);
        else
            return NULL;
    }
    
    if (pt && !strncmp(pt, "*DEL*", 5 ))
    {
        pt = NULL;
    }
    
    return pt;
}

char *nvram_default_get(const char *name)
{
    int i = 0;
    int j = 0;
    struct nvram_tuple *tail = NULL;
    
    tail = &router_defaults[i];
    while ((tail != NULL) && (tail->name != NULL))
    {
        if (name && router_defaults[i].name && (strcmp(name, router_defaults[i].name) == 0))
        {
            return (router_defaults[i].value != NULL) ? router_defaults[i].value:"";
        }
        tail = &router_defaults[++i];
    }
    
    tail = &router_state_defaults[j];
    while ((tail != NULL) && (tail->name != NULL))
    {
        if (name && router_state_defaults[j].name && (strcmp(name, router_state_defaults[j].name) == 0))
        {
            return (router_state_defaults[j].value != NULL) ? router_state_defaults[j].value:"";
        }
        tail = &router_state_defaults[++j];
    }
    
    return "";
}

static int nvram_set_func(const char* name,const char* value,char *path)
{
    char *bufspace, *targetspace;
    int size;
    char *sp, *s;
    int found=0;
    
    if ((size=readFile(path, &bufspace))>0)
    {
        targetspace=malloc(size+strlen(name)+strlen(value)+2);
    }
    else
    {
        targetspace=malloc(strlen(name)+strlen(value)+2);
    }
    sp=targetspace;
    if (size > 0)
    {
        for (s = bufspace; *s; s++)
        {
            if (!strncmp(s, name, strlen(name)) && *(s+strlen(name))=='=')
            {
                found=1;
                strcpy(sp, name);
                sp+=strlen(name);
                *(sp++) = '=';
                strcpy(sp, value);
                sp+=strlen(value);
                while (*(++s));
            }
            while (*s) *(sp++)=*(s++);
            *(sp++)=END_SYMBOL;
        }
        free(bufspace);
    }
    if (!found)
    {
        strcpy(sp, name);
        sp+=strlen(name);
        *(sp++) = '=';
        strcpy(sp, value);
        sp+=strlen(value);
        *(sp++) = END_SYMBOL;
    }
    *(sp) = END_SYMBOL;
    writeFile(path, targetspace, (sp-targetspace)+1);
    free(targetspace);
    return NVRAM_SUCCESS;
}

int nvram_set(const char* name,const char* value)
{
    if (strncmp(name, "other", 5) == 0)
    {
        return nvram_set_func(name, value, NVRAM_OTHER_CONF_PATH);
    }
    else
    {
        return nvram_set_func(name, value, NVRAM_TMP_PATH);
    }
}

void nvram_show_func(char *path)
{
    char *bufspace;
    int size;
    char *s,*pt;
    
    if ((size=readFile(path, &bufspace))<0)
    {
        return;
    }
    
    if (size > 0)
    {
        for (s = bufspace; *s; s++)
        {
            for(pt=strchr(s,0x01); pt; pt=strchr(pt+1,0x01)) *pt=0x20;
            printf("%s\n",s);
            while(*(++s));
        }
        free(bufspace);
    }
}

int nvram_contains_word(const char *key, const char *word)
{
    return (find_word(nvram_safe_get(key), word) != NULL);
}

int nvram_is_empty(const char *key)
{
    char *p;
    
    return (((p = nvram_get(key)) == NULL) || (*p == 0));
}

const char *find_word(const char *buffer, const char *word)
{
    const char *p, *q;
    int n;
    
    n = strlen(word);
    p = buffer;
    while ((p = strstr(p, word)) != NULL)
    {
        if ((p == buffer) || (*(p - 1) == ' ') || (*(p - 1) == ','))
        {
            q = p + n;
            if ((*q == ' ') || (*q == ',') || (*q == 0))
            {
                return p;
            }
        }
        ++p;
    }
    return NULL;
}

