/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef _NVRAM_
#define _NVRAM_

#include <string.h>

#define NVRAM_PATH     "/var/tmp/t.tmp"
#define NVRAM_TMP_PATH "/var/tmp/run"
#define NVRAM_DEFAULT  "/etc/default"
#define NVRAM_OTHER_CONF_PATH  "/var/tmp/.other_run"

#define END_SYMBOL          0x00
#define DIVISION_SYMBOL     0x01

#define NVRAM_MAGIC         0x48534C46

#define NVRAM_HEADER_SIZE   40
#define NVRAM_SIZE          65535

#define NVRAM_BUFF_SIZE     4096

#define NVRAM_SUCCESS       0
#define NVRAM_FLASH_ERR     1
#define NVRAM_MAGIC_ERR     2
#define NVRAM_LEN_ERR       3
#define NVRAM_CRC_ERR       4
#define NVRAM_SHADOW_ERR    5

typedef struct nvram_header_s
{
    unsigned long magic;
    unsigned long len;
    unsigned long crc;
    unsigned long reserved;
} nvram_header_t;

struct nvram_tuple
{
    char *name;
    char *value;
    struct nvram_tuple *next;
};

extern struct nvram_tuple router_defaults[];
extern struct nvram_tuple router_state_defaults[];

int nvram_load();
int nvram_commit();

char* nvram_get_func(const char *name,char *path);
#define nvram_get_def(name) nvram_get_func(name,NVRAM_DEFAULT)
#define nvram_safe_get(msg) (nvram_get(msg)?:nvram_default_get(msg)?:"")
char* nvram_get(const char *name);
char *nvram_default_get(const char *name);
int nvram_contains_word(const char *key, const char *word);
int nvram_is_empty(const char *key);
const char *find_word(const char *, const char *);

static inline int nvram_match(char *name, char *match)
{
    const char *value = nvram_safe_get(name);
    return (value && !strcmp(value, match));
}

static inline int nvram_invmatch(char *name, char *invmatch)
{
    const char *value = nvram_safe_get(name);
    return (value && strcmp(value, invmatch));
}

int nvram_set(const char* name,const char* value);

#endif

